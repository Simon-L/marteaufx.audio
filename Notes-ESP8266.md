# Murmurhash2 implementation for Arduino

The Heavy provided Murmurhash2 function does unaligned read/write on ESP8266 (core crashes with exception 9: https://arduino-esp8266.readthedocs.io/en/latest/exception_causes.html)  
Replace in HvUtils.c.
Tip: Replace in `hvcc/generators/ir2c/static/HvUtils.c` in order to make the change permanent, don't forget to change it back to the original!
Source: https://clarkli.wordpress.com/2012/12/12/murmur-hash-2-adapted-for-arduino/  

```c++
hv_uint32_t hv_string_to_hash(const char *key) {
  uint32_t len = hv_strlen(key);
  const uint32_t m = 0x5bd1e995;
  const uint32_t r = 24;
  uint32_t h = len;
  const unsigned char * data = (const unsigned char *)key;
  while(len >= 4)
  {
    uint32_t k;
    k = (uint32_t)data[0];
    k |= (uint32_t)data[1] << 8;
    k |= (uint32_t)data[2] << 16;
    k |= (uint32_t)data[3] << 24;
    k *= m;
    k ^= k >> r;
    k *= m;
    h *= m;
    h ^= k;
    data += 4;
    len -= 4;
  }

  switch(len)
  {
    case 3: h ^= (uint32_t)data[2] << 16;
    case 2: h ^= (uint32_t)data[1] << 8;
    case 1: h ^= (uint32_t)data[0];
    h *= m;
  };
  h ^= h >> 13;
  h *= m;
  h ^= h >> 15;
  return (hv_uint32_t)h;
}
```